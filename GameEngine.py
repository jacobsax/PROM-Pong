#Runs the pong game

from Plus import *
import Player
import random
import pygame
import LEDBar

import InputController

from PyGlow import PyGlow

import time

class GameEngine:
	
	PADDLE = 1
	BALL = 2
	NET = 3

	
	
	outputSize = Vector2D(80, 40)
			
	ballStep = 1.6 #the amount of distance the ball moves every loop
	
	paddleRes = 100
	
	def __init__(self):
		
		self.newGame() #Start a new game
		self.inputController = InputController.InputController()
		self.LEDBar = LEDBar.LEDBar()		

	def newGame(self):
		
		self.playerA = Player.Player(2)
		self.playerB = Player.Player(79)
		
		self.ballPosition = Vector2D(0, 0)
		self.ballAcceleration = Vector2D(0, 0)
				
		self.serveCount = 0
		
		self.inPlay = False #True if the ball is in play
		
		self.gameOver = False
				
	def getObjectPositions(self):
		
		return self.drawPaddle(self.playerA), self.drawPaddle(self.playerB), self.ballPosition 

	def getScreenOut(self): #return the screenOut 
		
		return self.screenOut

	#Updates the game	
	def update(self):
		
		self.inputController.update()
		
		self.LEDBar.setLed(int(mapToRange(80, 7, int(self.ballPosition.x))))
		
		playerAInput, playerBInput = self.inputController.getInput()
		
		#self.playerA.useAI(self.ballPosition.x, self.ballPosition.y)
		#self.playerB.useAI(self.ballPosition.x, self.ballPosition.y)
		
		#print(playerAInput .buttonAPressed)
		
		self.playerA.passInput(playerAInput.paddlePosition, playerAInput.buttonAPressed, playerAInput.buttonBPressed)
		self.playerB.passInput(playerBInput.paddlePosition, playerBInput.buttonAPressed, playerBInput.buttonBPressed)
		
		#If the ball currently isn't in play
		if (self.inPlay == False):
			#If it is player A's time to serve, and their button is pressed
			
			if (self.serveCount < 5):
				self.ballPosition = Vector2D(self.playerA.position.x + 1, self.playerA.position.y + 2)
				
				if (self.playerA.serve() == True):
				
					#self.serveCount += 1
					self.inPlay = True
					
					# set balls position and DX/DY
					self.startBall(self.playerA)
			
			#If it is player B's time to serve, and their button is pressed
			elif ((self.serveCount >= 5) and (self.serveCount < 10)):
				
				self.ballPosition = Vector2D(self.playerB.position.x - 1, self.playerB.position.y + 2)
				
				if (self.playerB.serve() == True):
				
					#self.serveCount += 1
					self.inPlay = True
					
					#set balls position and DX/DY
					self.startBall(self.playerB)

				
			#If the game is now over, but the scores have yet to be printed
			elif ((self.serveCount >= 10) and (self.gameOver == False)):
				self.gameOver = True
				
			#if the game is over and the scores have been printed, and any players button is pressed, start a new game
			elif ((self.gameOver == True) and ((self.playerA.serve() == True) or (self.playerB.serve() == True))):
				self.newGame()
				
				
		else: #If the ball is in play
			
			#if a point is scored
			if (self.isPointScored() == True):
				
				self.inPlay = False #set in play to False
				self.serveCount += 1
				
				if (self.ballPosition.x > self.outputSize.x / 2):
					self.playerA.score += 1
					
				else:
					self.playerB.score += 1
				
				pyglow = PyGlow()
				pyglow.color("white", 100)
				time.sleep(0.1)
				pyglow.color("blue", 100)
				time.sleep(0.1)
				pyglow.color("green", 100)
				time.sleep(0.1)
				pyglow.color("yellow", 100)
				time.sleep(0.1)
				pyglow.color("orange", 100)
				time.sleep(0.1)
				pyglow.color("red", 100)
				time.sleep(2)
				pyglow.all(0)
				
			else:
								
				#self.ballPosition = self.ballPosition + (self.ballAcceleration * self.ballStep) #update the ball position
								
				batCollision = self.checkBatCollision() #check if there has been a bat collision		

				if (not batCollision):
					self.checkEdgeCollision() #check if there has been an edge collision only if we haven't already had a bat collision
					

				self.ballPosition = self.ballPosition + (self.ballAcceleration * self.ballStep)
			
	#Starts the ball moving from the paddle size
	def startBall(self, player):
		if (player.position.x < self.outputSize.x / 2): #if we are on the left side of the board
			
			random.seed()
			
			#set forward motion and random vertical trajectory
			
			y = 0
			while (int (y) == 0):
				y = random.randrange(-1, 1)
			
			self.ballAcceleration = Vector2D(1, y)
			
		else: #if we are on the right side of the board
			
			random.seed()
			
			#set forward motion and random vertical trajectory
			
			y = 0
			while (int (y) == 0):
				y = random.randint(-1, 1)


			self.ballAcceleration = Vector2D(-1, y)
			
		
	#returns True if a point has been scored
	def isPointScored(self):
		if (self.ballPosition.x <= 1):
			return True
		elif (self.ballPosition.x >= self.outputSize.x):
			return True
		else:
			return False
	
	#checks if the ball has collided with a bat, if it has reverse the balls direction						
	def checkBatCollision(self):
		
		
		if ((int(self.ballPosition.y) >= int(self.playerA.paddleYMin())) and (int(self.ballPosition.y) <= int(self.playerA.paddleYMax())) and (int(self.ballPosition.x ) <= int(self.playerA.position.x))):
			self.ballAcceleration.x *= -1
			self.ballPosition.x += 1
			
			self.ballStep = random.uniform(0.8, 1.6)
			
			paddle_third = (self.playerA.paddleYMax() - self.playerA.paddleYMin()) / 3
			
			top_third = self.playerA.paddleYMin() + paddle_third
			mid_third = self.playerA.paddleYMin() + (2* paddle_third)
			bottom_third = self.playerA.paddleYMin() + (3 * paddle_third)

			if (self.ballPosition.y <= top_third):
				self.ballAcceleration.y = -1
			elif (self.ballPosition.y > top_third) and (self.ballPosition.y < mid_third):
				self.ballAcceleration.y = 0
			else:
				self.ballAcceleration.y = 1

			return True
			
		if ((int(self.ballPosition.y) >= int(self.playerB.paddleYMin())) and (int(self.ballPosition.y) <= int(self.playerB.paddleYMax())) and (int(self.ballPosition.x) >= int(self.playerB.position.x) - 1)):
			self.ballAcceleration.x *= -1		
			self.ballPosition.y += 1

			self.ballStep = random.uniform(0.8, 1.6)

			paddle_third = (self.playerB.paddleYMax() - self.playerB.paddleYMin()) / 3
			
			top_third = self.playerB.paddleYMin() + paddle_third
			mid_third = self.playerB.paddleYMin() + (2* paddle_third)
			bottom_third = self.playerB.paddleYMin() + (3 * paddle_third)

			if (self.ballPosition.y <= top_third):
				self.ballAcceleration.y = -1
			elif (self.ballPosition.y > top_third) and (self.ballPosition.y < mid_third):
				self.ballAcceleration.y = 0
			else:
				self.ballAcceleration.y = 1
			
			return True

		return False
							
	#checks if the ball has collided with an edge, if it has reverse its direction
	def checkEdgeCollision(self):
					
		if ((int(self.ballPosition.y) <= 0) or (int(self.ballPosition.y >= self.outputSize.y))):
			self.ballAcceleration.y *= -1
			
	def getScores(self):
		
		return self.playerA.score, self.playerB.score

		
	class paddle:
		
		def __init__(self):
			
			pass
		
		top = 0
		bottom = 0
		x = 0
		
	#draws the paddle on the screen
	def drawPaddle(self, player):
		
		paddle = self.paddle()
		
		paddle.top = player.paddleYMin()
		paddle.bottom = player.paddleYMax()
		
		paddle.bottom = limit(paddle.bottom, self.outputSize.y + 1)
		
		paddle.x = player.paddleX()
		
		return paddle
			
		
			
		
		
		
