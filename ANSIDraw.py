import time

RED = 41
BLACK = 40
GREEN = 42
YELLOW = 43
BLUE = 44
PURPLE = 45
CYAN = 46
WHITE = 47

BACKSPACE = chr(8)
DELETE = chr(127)

#used to generate an ANSI escape sequence to draw graphics to a screen
class ANSIDraw:
	
	#initialise the class
	def __init__(self, x, y):
		self.screenHeight = y + 1
		self.screenWidth = x + 1
		
		self.currentX = 1
		self.currentY = 1
		
		self.currentColour = WHITE
		
		#create a 2D array to hold objects on
		self.screen = [[WHITE for x in range(self.screenHeight + 1)] for x in range(self.screenWidth + 1)] 
		self.old_screen = [[BLACK for x in range(self.screenHeight + 1)] for x in range(self.screenWidth + 1)] 
		
		self.screen_change = [[-1 for x in range(self.screenHeight + 1)] for x in range(self.screenWidth + 1)]
		
	def fillScreen(self):
		return self.getBlocks(self.currentColour, (self.screenHeight - 1) * (self.screenWidth - 1))
		#self.currentX = 1
		#self.currentY = self.screenHeight + 1
		
		self.currentX = 1
		self.currentY = 1
		
	#set the current colour
	def setColour(self, colour):
		self.currentColour = colour
		
	def beginDraw(self):
		self.old_screen = self.screen
	
	def endDraw(self):
		self.screen_change = [[-1 for x in range(self.screenHeight + 1)] for x in range(self.screenWidth + 1)]
		
		for y in range (1, self.screenHeight):
			for x in range (1, self.screenWidth):
				if (self.screen[x][y] != self.old_screen[x][y]):
					self.screen_change[x][y] = self.screen[x][y]
		
	#Redraw the screen with a fresh background, of the colour currently set
	def drawBackground(self):
		self.screen = [[self.currentColour for x in range(self.screenHeight + 1)] for x in range(self.screenWidth + 1)] 
		
	def drawNumber(self, number, startX, startY):
		if (number == 0): #draw the number 0 on the screen
			self.screen[startX][startY] = self.currentColour
			self.screen[startX + 1][startY] = self.currentColour
			self.screen[startX + 2][startY] = self.currentColour

			self.screen[startX][startY + 1] = self.currentColour
			self.screen[startX][startY + 2] = self.currentColour
			self.screen[startX][startY + 3] = self.currentColour
			
			self.screen[startX + 2][startY + 1] = self.currentColour
			self.screen[startX + 2][startY + 2] = self.currentColour
			self.screen[startX + 2][startY + 3] = self.currentColour
			
			self.screen[startX][startY + 4] = self.currentColour
			self.screen[startX + 1][startY + 4] = self.currentColour
			self.screen[startX + 2][startY + 4] = self.currentColour
			
		elif (number == 1):
			self.screen[startX][startY] = self.currentColour
			self.screen[startX + 1][startY] = self.currentColour
			
			self.screen[startX + 1][startY + 1] = self.currentColour
			self.screen[startX + 1][startY + 2] = self.currentColour
			self.screen[startX + 1][startY + 3] = self.currentColour
			
			self.screen[startX][startY + 4] = self.currentColour
			self.screen[startX + 1][startY + 4] = self.currentColour
			self.screen[startX + 2][startY + 4] = self.currentColour
			
		elif (number == 2):
			self.screen[startX][startY] = self.currentColour
			self.screen[startX + 1][startY] = self.currentColour
			self.screen[startX + 2][startY] = self.currentColour
			self.screen[startX + 2][startY + 1] = self.currentColour
			self.screen[startX + 2][startY + 2] = self.currentColour
			self.screen[startX + 1][startY + 2] = self.currentColour
			self.screen[startX][startY + 2] = self.currentColour
			self.screen[startX][startY + 3] = self.currentColour
			self.screen[startX][startY + 4] = self.currentColour
			self.screen[startX + 1][startY + 4] = self.currentColour
			self.screen[startX + 2][startY + 4] = self.currentColour
			
		elif (number == 3):
			self.screen[startX][startY] = self.currentColour
			self.screen[startX + 1][startY] = self.currentColour
			self.screen[startX + 2][startY] = self.currentColour
			self.screen[startX + 2][startY + 1] = self.currentColour
			self.screen[startX + 2][startY + 2] = self.currentColour
			self.screen[startX + 2][startY + 3] = self.currentColour
			self.screen[startX + 2][startY + 4] = self.currentColour
			
			self.screen[startX][startY + 2] = self.currentColour
			self.screen[startX + 1][startY + 2] = self.currentColour
			
			self.screen[startX][startY + 4] = self.currentColour
			self.screen[startX + 1][startY + 4] = self.currentColour
			
		elif (number == 4):
			
			self.screen[startX - 1][startY + 3] = self.currentColour	
			self.screen[startX][startY + 3] = self.currentColour	
			self.screen[startX + 1][startY + 3] = self.currentColour	
			self.screen[startX + 2][startY + 3] = self.currentColour	
			self.screen[startX + 3][startY + 3] = self.currentColour
			
			self.screen[startX + 2][startY + 0] = self.currentColour	
			self.screen[startX + 2][startY + 1] = self.currentColour	
			self.screen[startX + 2][startY + 2] = self.currentColour	
			self.screen[startX + 2][startY + 4] = self.currentColour	
			
			self.screen[startX ][startY + 2] = self.currentColour	
			self.screen[startX + 1][startY + 1] = self.currentColour	
			
		elif (number == 5):
			self.screen[startX][startY] = self.currentColour	
			self.screen[startX + 1][startY] = self.currentColour
			self.screen[startX + 2][startY] = self.currentColour
			self.screen[startX][startY + 1] = self.currentColour
			self.screen[startX + 2][startY + 2] = self.currentColour
			self.screen[startX + 1][startY + 2] = self.currentColour
			self.screen[startX][startY + 2] = self.currentColour
			self.screen[startX + 2][startY + 3] = self.currentColour
			self.screen[startX + 2][startY + 4] = self.currentColour
			self.screen[startX][startY + 4] = self.currentColour
			self.screen[startX + 1][startY + 4] = self.currentColour
			self.screen[startX + 2][startY + 4] = self.currentColour
			
		elif (number == 6):
			self.screen[startX][startY] = self.currentColour	
			self.screen[startX + 1][startY] = self.currentColour
			self.screen[startX + 2][startY] = self.currentColour
			self.screen[startX][startY + 1] = self.currentColour
			self.screen[startX + 2][startY + 2] = self.currentColour
			self.screen[startX + 1][startY + 2] = self.currentColour
			self.screen[startX][startY + 2] = self.currentColour
			self.screen[startX][startY + 3] = self.currentColour
			self.screen[startX + 2][startY + 3] = self.currentColour
			self.screen[startX + 2][startY + 4] = self.currentColour
			self.screen[startX][startY + 4] = self.currentColour
			self.screen[startX + 1][startY + 4] = self.currentColour
			self.screen[startX + 2][startY + 4] = self.currentColour
			
		elif (number == 7):
			self.screen[startX - 1][startY] = self.currentColour
			self.screen[startX][startY] = self.currentColour
			self.screen[startX + 1][startY] = self.currentColour
			self.screen[startX + 2][startY] = self.currentColour
			self.screen[startX + 2][startY + 1] = self.currentColour
			self.screen[startX + 1][startY + 2] = self.currentColour
			self.screen[startX][startY + 3] = self.currentColour
			self.screen[startX - 1][startY + 4] = self.currentColour
			
		elif (number == 8):
			
			self.screen[startX][startY] = self.currentColour
			self.screen[startX + 1][startY] = self.currentColour
			self.screen[startX + 2][startY] = self.currentColour

			self.screen[startX][startY + 1] = self.currentColour
			self.screen[startX][startY + 2] = self.currentColour
			self.screen[startX][startY + 3] = self.currentColour
			
			self.screen[startX + 2][startY + 1] = self.currentColour
			self.screen[startX + 2][startY + 2] = self.currentColour
			self.screen[startX + 2][startY + 3] = self.currentColour
			
			self.screen[startX][startY + 4] = self.currentColour
			self.screen[startX + 1][startY + 4] = self.currentColour
			self.screen[startX + 2][startY + 4] = self.currentColour
			
			self.screen[startX + 1][startY + 2] = self.currentColour
		
		elif (number == 9):
			
			self.screen[startX][startY] = self.currentColour
			self.screen[startX + 1][startY] = self.currentColour
			self.screen[startX + 2][startY] = self.currentColour

			self.screen[startX][startY + 1] = self.currentColour
			self.screen[startX][startY + 2] = self.currentColour
			
			self.screen[startX + 2][startY + 1] = self.currentColour
			self.screen[startX + 2][startY + 2] = self.currentColour
			self.screen[startX + 2][startY + 3] = self.currentColour
			
			self.screen[startX + 2][startY + 4] = self.currentColour
			
			self.screen[startX + 1][startY + 2] = self.currentColour
			
	#draw a rectangle of the colour currently set
	def rectangle(self, startX, startY, endX, endY):
		for y in range(int(startY + 1), int(endY + 1)):
			for x in range(int(startX), int(endX) ):
				self.screen[x][y] = self.currentColour
	
	def replaceBlock(self, x, y, colour):
		
		line = ""

		line += "\033[" + str(y) +";" + str(x) + "H"

		
		line += chr(127)
		
		line += self.getBlocks(colour, 1)
		
		return line
	
	#returns a ANSI string of blocks of a single colour			
	def getBlocks(self, colour, length):
		line = "\033[5;41;" + str(colour) + "m"
		
		for x in range(0, length): #a block is made up of two spaces
			line += " "
			
		line += "\033[0m"
		
		return line
	
	#returns a single string containing all ANSI characters needed to draw the screen			
	def generateOutput(self):
		string = ""
		
		for y in range (1, self.screenHeight):
			
			currentColour = WHITE
			squareLength = 0
			
			for x in range (1, self.screenWidth):
				if self.screen_change[x][y] != -1:
					
					string += self.replaceBlock(x, y, self.screen_change[x][y])
					
		return string

