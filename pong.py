#!/usr/bin/python

from GameEngine import *
import InputController
import ANSIDraw

import time
import DisplayController


Game = GameEngine()
screen = ANSIDraw.ANSIDraw(80, 40)
display = DisplayController.DisplayController()
display.openSerial("/dev/ttyAMA0", 115200)

display.outputToSerial(screen.fillScreen())

flash = False
flashTimer = 0
flashTime = 0.5

while True:
	Game.update()
	
	#get the position of all the game elements
	paddleA, paddleB, ball = Game.getObjectPositions()
	playerAScore, playerBScore = Game.getScores()
	
	#wipe the screen by redawing the background
	screen.beginDraw()
	screen.setColour(ANSIDraw.WHITE)
	screen.drawBackground()
	
	screen.setColour(ANSIDraw.CYAN)
	
	
	if (playerAScore <= 9):
		screen.drawNumber(playerAScore, 32, 3)
	elif (playerAScore == 10):
		screen.drawNumber(1, 27, 3)
		screen.drawNumber(0, 32, 3)

	if (playerBScore <= 9):
		screen.drawNumber(playerBScore, 46, 3)
	elif (playerBScore == 10):
		screen.drawNumber(1, 46, 3)
		screen.drawNumber(0, 51, 3)

	if (playerAScore + playerBScore == 10):
		screen.setColour(ANSIDraw.WHITE)

		if (flashTimer + flashTime < time.time()):
			flashTimer = time.time()
			
			if (flash):
				flash = False
			else:
				flash = True
			
		if (flash):
			if (playerAScore > playerBScore):
				screen.rectangle(26, 0, 36, 10)
			elif (playerBScore > playerAScore):
				screen.rectangle(45, 0, 55, 10)

	#draw both paddles
	screen.setColour(ANSIDraw.RED)
	screen.rectangle(paddleA.x, paddleA.top, paddleA.x + 1, paddleA.bottom)
	screen.rectangle(paddleB.x, paddleB.top, paddleB.x + 1, paddleB.bottom)
	
	#draw the net
	screen.setColour(ANSIDraw.BLUE)
	screen.rectangle(40, 2, 41, 4)
	screen.rectangle(40, 6, 41, 8)
	screen.rectangle(40, 10, 41, 12)
	screen.rectangle(40, 14, 41, 16)
	screen.rectangle(40, 18, 41, 20)
	screen.rectangle(40, 22, 41, 24)
	screen.rectangle(40, 26, 41, 28)
	screen.rectangle(40, 30, 41, 32)
	screen.rectangle(40, 34, 41, 36)
	screen.rectangle(40, 38, 41, 40)
	
	#draw the ball
	screen.setColour(ANSIDraw.BLUE)
	if (ball.y <= 0):
		ball.y = 0
	elif (int(ball.y) >= 40):
		ball.y = 40

	screen.rectangle(ball.x, ball.y, ball.x + 1, ball.y + 1)
	
	screen.endDraw()
	
	output = screen.generateOutput()

	if (output != ''):
		display.outputToSerial(output) #outputs the game display to the serial line



