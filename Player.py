from Plus import *
import time

class Player:
	
	def __init__(self, x_start):
		
		self.position = Vector2D(x_start, 8)
		self.length = 4
		self.doubleLength = False
		self.doubleLengthCount = 0
		self.score = 0
		self.buttonA = False
		self.buttonB = False
		self.startTime = 0
		
	def passInput(self, paddlePosition, buttonA, buttonB):
		self.position.y = paddlePosition
		self.buttonA = buttonA
		self.buttonB = buttonB
		
		if (buttonB == True):
			if ((self.doubleLength == False) and (self.doubleLengthCount < 2)):
				self.doubleLength = True
				self.startTime = time.time()
				self.doubleLengthCount += 1
				print("double length activated")
				
		if (self.doubleLength == True):
			if (self.startTime + 15 < time.time()):
				self.doubleLength = False
		
	def useAI(self, ballX, ballY):
		if abs(ballX - self.position.x) < 10:
			if (self.position.y + 2 - ballY) > 0:
				self.position.y -= 1
			elif (self.position.y + 2 - ballY) < 0:
				self.position.y += 1
		
	def paddleYMin(self):
		
		
		
		if (self.doubleLength == True):
			return self.position.y - 2
			
		else:
			
			return self.position.y 
		
		
	def paddleYMax(self):
		
		
		if (self.doubleLength == True):
			return self.position.y + self.length + 2
		else:
			return self.position.y + self.length
		
	def paddleX(self):
		
		return self.position.x
		
	def resetDoubleLength(self):
		
		self.doubleLengthUsed = False
				
	def serve(self):
		return self.buttonA
		
		