#!/usr/bin/python

import smbus #I2C library
import time #Time library
import math
	
class DigitalInput:
	
	I2C_ADDR = 0x38 #I2C base address
	PORT_ON = 0xFF
	READ_SIGNAL = 0x00 # if not this try 0xFF
	
	def __init__(self, pin):
		self.pin = pin
		self.bus = smbus.SMBus(1) #enable I2C bus
		self.bus.write_byte( I2C_ADDR,  PORT_ON)
	
	def getState(self):
		self.bus.write_byte( I2C_ADDR,  READ_SIGNAL)
		i2cvalue = self.bus.read_byte( I2C_ADDR )
		
		Filter = pow(self.pin, 2) #get the filter for the required pin
		
		if ((dataIn & Filter) > 0):
			return True
		else:
			return False
			
button = DigitalInput(0)

while True:
	print (button.getState())
	time.sleep(0.5)