#!/usr/bin/python

import smbus
import time

PIN_2 = 0x20
PIN_4 = 0x80
PIN_3 = 0x40
PIN_1 = 0x10

I2CADDR = 0x21

class AnalogueInput:
	
	def __init__(self, pin):
		
		self.pin = pin
		
		self.bus = smbus.SMBus(1)
		
	def getValue(self):
		self.bus.write_byte( I2CADDR, self.pin)
		tmp = self.bus.read_word_data( I2CADDR, 0x00 ) 
		
		return self.flip_bytes(tmp) & 0xfff

	def flip_bytes(self, val):
		return ((val << 8) & 0xFF00) | ((val >>  8) & 0x00FF)
