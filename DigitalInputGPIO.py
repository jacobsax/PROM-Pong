#!/usr/bin/python

import time #Time library
import RPi.GPIO as GPIO

PLAYER_B_BOTTOM = 4
PLAYER_B_TOP = 18

PLAYER_A_BOTTOM = 11
PLAYER_A_TOP = 17 

	
class DigitalInput:

	oldState = False
	state = False
	count = 0
	requiredCount = 3
		
	def __init__(self, pin):
		self.pin = pin
		GPIO.setmode(GPIO.BCM)
		GPIO.setup(pin, GPIO.IN)
		
			
	def getState(self):
		#simple software debounce for the button
		#self.old_state = self.state
		self.state = GPIO.input(self.pin)
		
		if (self.state):
			self.count += 1
			if (self.count >= self.requiredCount):
				return True
		else:
			self.count = 0
			return False
		
		#if (self.old_state == self.state):
			#return self.state
