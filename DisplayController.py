#Displays the pong game on the screen, used to manage the Serial output

from serial import Serial
import time

class DisplayController:
	
	def __init__(self):
		
		pass
		
	def openSerial(self, port, baud):
	
		# Open Pi serial port, speed 9600 bits per second
		self.serialPort = Serial(port, baud)
		
		# Should not need, but just in case
		if (self.serialPort.isOpen() == False):
		    self.serialPort.open()
				
	def closeSerial(self):
		self.serialPort.close()
		
	def outputToSerial(self, text):
		
		#self.serialPort.write(bytes(text,'UTF-8'))
		self.serialPort.write(text)
		time.sleep(0.05)		
